create_table = {
    "name":"Members",
    "data":[
        {"name":"id", "type":"INTEGER", "not_null":"true"},
        {"name":"name", "type":"TEXT", "not_null":"true"},
        {"name":"discord_id", "type":"INTEGER", "not_null":"true"},
        {"name":"sinisforas", "type":"INTEGER", "not_null":"true"},
        {"name":"quality", "type":"INTEGER", "not_null":"true"}
    ],
    "primary_key":{"column":"id", "Auto_Increment":"true"},
    "foreign_key":[]
}
search_by_id = {
    "column":"*",
    "table":"Members",
    "option":[
        {"column":"id","data":{"format":"search_input","criteria":""}}
    ],
    "seperator_option":" AND ",
    "type_key":int
}
search_by_discord_id = {
    "column":"*",
    "table":"Members",
    "option":[
        {"column":"discord_id","data":{"format":"search_input","criteria":""}}
    ],
    "seperator_option":" AND ",
    "type_key":int
}
search_by_name = {
    "column":"*",
    "table":"Members",
    "option":[
        {"column":"name","data":{"format":"search_input","criteria":""}}
    ],
    "seperator_option":" AND ",
    "type_key":str
}
insert_update = {
    "table":"Members",
    "data":[
        "name",
        "discord_id",
        "sinisforas",
        "quality"
    ],
    "string":[
        "name"
    ],
    "cp":"id"
}