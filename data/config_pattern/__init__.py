import data.config_pattern.setup_items as Items
import data.config_pattern.setup_bdg as BDG
import data.config_pattern.setup_member as Member
setup_db = []
setup_db.append(Items.create_table)
setup_db.append(BDG.create_table)
setup_db.append(Member.create_table)