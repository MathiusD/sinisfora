create_table = {
    "name":"Items",
    "data":[
        {"name":"id", "type":"INTEGER", "not_null":"true"},
        {"name":"name", "type":"TEXT", "not_null":"true"},
        {"name":"quality", "type":"INTEGER", "not_null":"true"},
        {"name":"level", "type":"INTEGER", "not_null":"true"}
    ],
    "primary_key":{"column":"id", "Auto_Increment":"true"},
    "foreign_key":[]
}
search = {
    "column":"*",
    "table":"Items",
    "option":[
        {"column":"id","data":{"format":"search_input","criteria":""}}
    ],
    "seperator_option":" AND ",
    "type_key":int
}
insert_update = {
    "table":"Items",
    "data":[
        "id",
        "name",
        "quality",
        "level"
    ],
    "string":[
        "name"
    ],
    "cp":"id"
}