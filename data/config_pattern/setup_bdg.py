create_table = {
    "name":"BDG",
    "data":[
        {"name":"id", "type":"INTEGER", "not_null":"true"},
        {"name":"items", "type":"INTEGER", "not_null":"true"},
        {"name":"number", "type":"INTEGER", "not_null":"true"}
    ],
    "primary_key":{"column":"id", "Auto_Increment":"true"},
    "foreign_key":[
        {
            "column":"items",
            "cible":{"table":"Items", "column":"id"}
        }
    ]
}
search = {
    "column":"*",
    "table":"BDG",
    "option":[
        {"column":"id","data":{"format":"search_input","criteria":""}}
    ],
    "seperator_option":" AND ",
    "type_key":int
}
insert_update = {
    "table":"Items",
    "data":[
        "id",
        "items",
        "number"
    ],
    "string":[],
    "cp":"id"
}