#!/bin/bash

FILE=./data/settings.py
if ! [ -f "$FILE" ]; then
    cp ./data/settings.sample.py $FILE;
fi
git clone https://gitlab.com/MathiusD/owninstall;
cp owninstall/install.py own_install.py;
python3 build.py
rm -rf owninstall
