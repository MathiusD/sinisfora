import discord, time, random, os, logging
from discord.ext import commands
from own.own_discord import *
from own.own_sqlite import *
from data import sqlite
from data import settings

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', filename='logs/%s.log'%(str(time.time())), level=settings.DISCORD_LOG)
bot = commands.Bot(command_prefix=settings.DISCORD_PREFIX, description=settings.DISCORD_DESC)

logging.info("Verification Database")
if (bdd_exist(settings.DISCORD_DB) != True):
    logging.info("Creating Database")
    bdd_create(settings.DISCORD_DB)

logging.info("Connecting Database")
bdd = connect(settings.DISCORD_DB)

@bot.event
async def on_ready():
    for table in sqlite.setup_db:
        logging.info("Verification Table " + str(table['name']))
        if (table_exist(table['name'], bdd) != True):
            logging.info("Creating Table " + str(table['name']))
            table_create(table['name'], table, bdd)
    logging.info("Bot Launch.")

@bot.command(name="SignUp")
async def SignUp(ctx, name=None):
    user = ctx.message.author.id
    if not name:
        name = ctx.message.author.nick
    if len(data_extract(user, sqlite.Member.search_by_discord_id, bdd)) == 0:
        if len(data_extract(name, sqlite.Member.search_by_name, bdd)) == 0:
            if data_insert({"name":name,"discord_id":user,"sinisforas":0}, sqlite.Member.insert_update, bdd):
                msg = "Account created !"
            else:
                msg = "Account not created (Internal Error, please Report error)."
        else:
            msg = "Account not available."
    else:
        msg = "You already have an account."
    await reponse_send(ctx, msg)

bot.run(settings.DISCORD_TOKEN)